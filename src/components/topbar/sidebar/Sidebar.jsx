import React from 'react';
import "./sidebar.css";
import LineStyleIcon from '@mui/icons-material/LineStyle';
import TimelineIcon from '@mui/icons-material/Timeline';
import TrendingUpIcon from '@mui/icons-material/TrendingUp';
import ReportIcon from '@mui/icons-material/Report';
import ManageHistoryIcon from '@mui/icons-material/ManageHistory';
import MessageIcon from '@mui/icons-material/Message';
import EmailIcon from '@mui/icons-material/Email';
import DynamicFeedIcon from '@mui/icons-material/DynamicFeed';
import MonetizationOnIcon from '@mui/icons-material/MonetizationOn';
import PersonIcon from '@mui/icons-material/Person';
import InventoryIcon from '@mui/icons-material/Inventory';
import BarChartIcon from '@mui/icons-material/BarChart';

export default function Sidebar() {
  return (
    <div className='sidebar'>
        <div className="sidebarWrapper">
            <div className="sidebarMenu">
                <h3 className="sidebarTitle">
                    Dashboard
                </h3>
                <ul className="sidebarList">
                    <li className="sidebarListItem active">
                        <LineStyleIcon className='sidebarIcon'/>
                        Home
                    </li>
                    <li className="sidebarListItem">
                        <TimelineIcon className='sidebarIcon'/>
                        Analytics
                    </li>
                    <li className="sidebarListItem">
                        <TrendingUpIcon className='sidebarIcon'/>
                        Sales
                    </li>
                </ul>
                
            </div>
            <div className="sidebarMenu">
                <h3 className="sidebarTitle">
                    Quick Menu
                </h3>
                <ul className="sidebarList">
                    <li className="sidebarListItem ">
                        <PersonIcon className='sidebarIcon'/>
                        Users
                    </li>
                    <li className="sidebarListItem">
                        <InventoryIcon className='sidebarIcon'/>
                        Products
                    </li>
                    <li className="sidebarListItem">
                        <MonetizationOnIcon className='sidebarIcon'/>
                        Transactions
                    </li>
                    <li className="sidebarListItem">
                        <BarChartIcon className='sidebarIcon'/>
                        Reports
                    </li>
                </ul>
                
            </div>
            <div className="sidebarMenu">
                <h3 className="sidebarTitle">
                    Notification
                </h3>
                <ul className="sidebarList">
                    <li className="sidebarListItem ">
                        <EmailIcon className='sidebarIcon'/>
                        Mail
                    </li>
                    <li className="sidebarListItem">
                        <DynamicFeedIcon className='sidebarIcon'/>
                        Feedback
                    </li>
                    <li className="sidebarListItem">
                        <MessageIcon className='sidebarIcon'/>
                        Messages
                    </li>
                </ul>
                
            </div>
            <div className="sidebarMenu">
                <h3 className="sidebarTitle">
                    Staff
                </h3>
                <ul className="sidebarList">
                    <li className="sidebarListItem ">
                        <ManageHistoryIcon className='sidebarIcon'/>
                        Manage
                    </li>
                    <li className="sidebarListItem">
                        <TimelineIcon className='sidebarIcon'/>
                        Analytics
                    </li>
                    <li className="sidebarListItem">
                        <ReportIcon className='sidebarIcon'/>
                        Reports
                    </li>
                </ul>
                
            </div>
        </div>
    </div>
  )
}

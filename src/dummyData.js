export const userData = [
    {
      name: 'Jan',
      "Active User": 4000,
    },
    {
      name: 'Feb',
      "Active User": 3000,
    },
    {
      name: 'Mar',
      "Active User": 5000,
    },
    {
      name: 'Apr',
      "Active User": 4000,
    },
    {
      name: 'May',
      "Active User": 6000,
    },
    {
      name: 'Jun',
      "Active User": 5000,
    },
    {
      name: 'Jul',
      "Active User": 7000,
    },
    {
      name: 'Aug',
      "Active User": 4000,
    },
    {
      name: 'Sep',
      "Active User": 2000,
    },
    {
      name: 'Oct',
      "Active User": 6000,
    },
    {
      name: 'Nov',
      "Active User": 7000,
    },
    {
      name: 'Dec',
      "Active User": 5000,
    },
  ];

  export const userRows = [
    {
      id: 1,
      username: " John Snow",
      avatar:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRY0279wY8MI2bCZ4NTC_duQIB2n61pB5zhXQ&usqp=CAU",
      email: "john@gmail.com",
      status: "active",
      transaction: "$120.00",
    },
    {
      id: 2,
      username: " John Snow",
      avatar:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRY0279wY8MI2bCZ4NTC_duQIB2n61pB5zhXQ&usqp=CAU",
      email: "john@gmail.com",
      status: "active",
      transaction: "$120.00",
    },
    {
      id: 3,
      username: " John Snow",
      avatar:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRY0279wY8MI2bCZ4NTC_duQIB2n61pB5zhXQ&usqp=CAU",
      email: "john@gmail.com",
      status: "active",
      transaction: "$120.00",
    },
    {
      id: 4,
      username: " John Snow",
      avatar:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRY0279wY8MI2bCZ4NTC_duQIB2n61pB5zhXQ&usqp=CAU",
      email: "john@gmail.com",
      status: "active",
      transaction: "$120.00",
    },
    {
      id: 5,
      username: " John Snow",
      avatar:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRY0279wY8MI2bCZ4NTC_duQIB2n61pB5zhXQ&usqp=CAU",
      email: "john@gmail.com",
      status: "active",
      transaction: "$120.00",
    },
    {
      id: 6,
      username: " John Snow",
      avatar:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRY0279wY8MI2bCZ4NTC_duQIB2n61pB5zhXQ&usqp=CAU",
      email: "john@gmail.com",
      status: "active",
      transaction: "$120.00",
    },
    {
      id: 7,
      username: " John Snow",
      avatar:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRY0279wY8MI2bCZ4NTC_duQIB2n61pB5zhXQ&usqp=CAU",
      email: "john@gmail.com",
      status: "active",
      transaction: "$120.00",
    },
    {
      id: 8,
      username: " John Snow",
      avatar:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRY0279wY8MI2bCZ4NTC_duQIB2n61pB5zhXQ&usqp=CAU",
      email: "john@gmail.com",
      status: "active",
      transaction: "$120.00",
    },
    {
      id: 9,
      username: " John Snow",
      avatar:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRY0279wY8MI2bCZ4NTC_duQIB2n61pB5zhXQ&usqp=CAU",
      email: "john@gmail.com",
      status: "active",
      transaction: "$120.00",
    },
  ];